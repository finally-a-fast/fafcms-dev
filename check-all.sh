#!/bin/bash
find . -maxdepth 1 -type d -exec printf "{}\n" \; -exec git -C {} status -s \; -exec printf "\n" \;

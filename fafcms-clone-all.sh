#!/bin/bash

projects=$(curl -s 'https://gitlab.com/api/v4/groups/finally-a-fast/projects?per_page=1000')

for row in $(echo "${projects}" | jq -r '.[] | @base64'); do
    _jq() {
     echo ${row} | base64 --decode | jq -r ${1}
    }

   git clone $(_jq '.ssh_url_to_repo')
done

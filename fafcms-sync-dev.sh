#!/bin/bash

BASEDIR=$(dirname "$0")/
PROJECTS_DIR="./"
FAFCMS_CORE="fafcms-core"


red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`


if [[ -d "$BASEDIR$PROJECTS_DIR$FAFCMS_CORE" ]]
then
    echo -e "${green}fafcms-core found at $BASEDIR$PROJECTS_DIR$FAFCMS_CORE${reset}\n\n"
else
    if [[ -d "$BASEDIR../$FAFCMS_CORE" ]]
    then
        PROJECTS_DIR="../"
        echo -e "${green}fafcms-core found at $BASEDIR$PROJECTS_DIR$FAFCMS_CORE${reset}\n\n"
    else
        echo -e "${red}fafcms-core not found to identify starting directory${reset}"
        exit 1
    fi
fi


for project in $BASEDIR$PROJECTS_DIR*/
do
    #if [[ -d "$project" && ! -L "$project" ]]
    if [[ -d "$project" ]]
    then
        PROJECT_CLEAN=`git -C $project status -s 2> /dev/null`
        if [ ! -n "$PROJECT_CLEAN" ]
        then
            echo "$project is ${green}clean${reset} ... pulling"
            git --git-dir=$project/.git --work-tree=$project pull
        else
            echo "$project is ${red}unclean${reset}"
            read -p "Do you want to add all new files, commit and push the project \"$project\"? [y]es / [n]o / [a]bort = " commitquestion
            case $commitquestion in
                [Yy]* ) git --git-dir=$project/.git --work-tree=$project pull; git --git-dir=$project/.git --work-tree=$project add --all; git --git-dir=$project/.git --work-tree=$project commit; git --git-dir=$project/.git --work-tree=$project push; continue;;
                [Nn]* ) continue;;
                [Aa]* ) exit;;
                * ) echo "Please answer [y]es or [n]o or [a]bort.";;
            esac
        fi
    fi
done


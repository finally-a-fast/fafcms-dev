[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Development tools
================================================

This repository only contains internal development tools to develop the Finally a fast CMS (FAFCMS)! Please don't use this if you don't know what you're doing.
For FAFCMS plugin development and debugging please install [finally-a-fast/fafcms-module-developer-tools](https://gitlab.com/finally-a-fast/fafcms-module-developer-tools) with composer.

[[_TOC_]]

Current Scripts
---------------

### Recommended
#### fafcms-clone-all.sh
Clones all finally-a-fast group projects at gitlab.
To run it excecute `./fafcms-dev/fafcms-clone-all.sh` inside of the target dir.

Requires `jq`! You can install it with `apt install jq` or on mac `brew install jq` see [https://stedolan.github.io/jq/download/](https://stedolan.github.io/jq/download/) for details.

#### fafcms-sync-dev.sh
Interactive script which combines both scripts. It keeps your lokal files up to date and asks you if you want to commit new changes.
To run it excecute `./fafcms-dev/fafcms-sync-dev.sh` in the base FAFCMS folder which contains all FAFCMS module repositories.

### Outdated
#### check-all.sh
Runs `git status` for each folder to check if there uncommited changes.
To run it excecute `./fafcms-dev/check-all.sh` in the base FAFCMS folder which contains all FAFCMS module repositories.

#### update-all.sh
Runs `git pull` for each folder to keep your lokal files up to date.
To run it excecute `./fafcms-dev/update-all.sh` in the base FAFCMS folder which contains all FAFCMS module repositories.

License
-------

**fafcms-dev** is released under the MIT License. See the [LICENSE.md](LICENSE.md) for details.
